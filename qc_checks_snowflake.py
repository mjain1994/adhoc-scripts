# ___Author:Neeraj
# ___Script to get QC errors for count and unique checks on tables

# !/usr/bin/env python3

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import datetime
import json
import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import snowflake.connector
from pyhive import presto
from sqlalchemy import Column, DateTime, String, Integer, ForeignKey, func, Boolean
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.orm import sessionmaker

from config_presto import presto_password, presto_username, email_password
import pandas as pd


Base = declarative_base()
current_check = None

##Initialize Sql Alchemy engine
engine = create_engine('mysql://qc_db_root:X3uLUWHJ8HnBeRHe@qc-checks-db.ckvce9fjaook.ap-southeast-1.rds.amazonaws.com:3306/qc_check_db')
#engine = create_engine('mysql://qc_db_root:X3uLUWHJ8HnBeRHe@127.0.0.1:8002/qc_check_db')
session = sessionmaker()
session.configure(bind=engine)
Base.metadata.create_all(engine)
s1 = session()


class Check(Base):
    __tablename__ = 'qc_check_snowflake_v3'
    id = Column(Integer, primary_key=True)
    table_nm = Column(String)
    schema_nm = Column(String)
    error_warning = Column(String)
    qc_check = Column(String)
    info = Column(String)
    is_active = Column(Boolean)
    created_on = Column(DateTime, default=func.now())


class Result(Base):
    __tablename__ = 'result_snowflake_v3'
    id = Column(Integer, primary_key=True)
    result = Column(Boolean)
    description = Column(String)
    check_id = Column(Integer, ForeignKey('qc_check_snowflake_v3.id'), onupdate='CASCADE')
    created_on = Column(DateTime, default=func.now())
    check = relationship('Check', backref='results')


# Connection to Presto

def createPrestoConn(catalog, schema):
    host = 'apg.swiggy.com'
    port = 24563
    user = presto_username
    catalog = catalog
    schema = schema
    password = presto_password
    __conn = None
    __conn = presto.connect(host, port=port,
                            username=user, password=password,
                            catalog=catalog, schema=schema, protocol='https')
    return __conn


def createSnowflakeConn():
    print("Creating snowflake connection")
    __conn = snowflake.connector.connect(
        user="qc_check_user",
        password="KXBnTMmwXTwFY4EN",
        account="swiggy-caifuhmyskbpytwlscdfskwp3sfya.global",
        role="SYSADMIN",
        warehouse="LOAD_WH"
    )
    return __conn


class WarehouseCursor(object):
    def __init__(self, mode, catalog='hive', schema='ETLJOBS'):
        self.__cursor = None
        self.__conn = None
        if mode == 'snowflake':
            self.__conn = createSnowflakeConn()
        else:
            self.__conn = createPrestoConn(catalog, schema)
        self.__cursor = self.__conn.cursor()

    def execute_query(self, query):
        print(query)
        try:
            self.__cursor.execute(query)
        except Exception as e:
            raise e

    def get_data(self, query):
        print(query)
        f_query.write(query)
        f_query.write("\n \n \n ")
        try:
            self.__cursor.execute(query)
            res = self.__cursor.fetchone()
            return res
        except Exception as e:
            f_err.write(query)
            f_err.write("\n \n \n exception below \n \n")
            f_err.write(str(e))
            raise e

    def get_description(self, query1):
        print(query1)
        f_query.write(query1)
        f_query.write("\n \n \n")
        try:
            self.__cursor.execute(query1)
            self.__cursor.description()
        except Exception as ex:
            print(ex)

    def close_con(self):
        self.__conn.close()


def write_result_to_db(result, description, data_cursor5):
    if not description.isdigit():
        sf_query = "INSERT INTO DP.DATA_ENGG.QC_CHECK_SNOWFLAKE(result, description, created_on) VALUES({},".format(result) + "'{}'".format(description.replace("'", "")) + ", to_timestamp_ntz(current_timestamp))"
        data_cursor5.execute_query(sf_query)
    result = Result(result=result, description=description, check=current_check)
    s1.add(result)
    s1.commit()


def get_count_error(data_cursor0, table_nm, date_load, date_flg, error_warn_count, dt_col='dt'):
    if date_flg == 1:
        date_type_qry = "select %stypeof(%s) from %s where %s = dateadd('day', -1, current_date) limit 1" % ('system$', dt_col, table_nm, dt_col)
        date_type = data_cursor0.get_data(date_type_qry)
        if date_type is None or date_type[0] != 'date':
            query = 'select count(1) from ' + table_nm + ' where %s = ' % (dt_col) + date_load
        else:
            query = 'select count(1) from ' + table_nm + ' where %s = date ' % (dt_col) + date_load
    else:
        query = 'select count(1) from ' + table_nm
    result = data_cursor0.get_data(query)

    f_query.write(" Executed \n \n \n")
    if result[0] == 0:
        print('table has  zero data')
        err_strng_count = table_nm + "," + ' count_check ' + "," + ' Table has 0 data for date {load_date}'.format(
            load_date=date_load) + "\n"
        # f_err.write(err_strng_count)
        print_error(error_warn_count, err_strng_count)
        write_result_to_db(False, err_strng_count, data_cursor0)
    else:
        suceess_strng_count = table_nm + "," + ' count_check ' + "," + ' Success ' + "\n"
        f_success.write(suceess_strng_count)
        write_result_to_db(True, str(result[0]), data_cursor0)


def get_unique_error(data_cursor1, table_nm, columns, date_load, date_flg, error_warn_unique):
    if date_flg == 1:
        date_type_qry = 'select system$typeof(dt) from ' + table_nm + " where dt = dateadd('day', -1, current_date) limit 1"
        date_type = data_cursor1.get_data(date_type_qry)
        if date_type is None:
            date_type_qry = 'select system$typeof(dt) from ' + table_nm + ' limit 1'
            date_type = data_cursor1.get_data(date_type_qry)
        # if date_type[0] != 'date':
        ##    query = 'select ' + columns + ' ,count(1) from ' + table_nm + ' where dt= ' + date_load + ' group by ' + columns + ' having count(1) > 1 '
        ##   query = 'select ' + columns + ' ,count(1) from ' + table_nm + ' where dt= date ' + date_load + ' group by ' + columns + ' having count(1) > 1 '
    # else:
    # query = 'select ' + columns + ' ,count(1) from ' + table_nm + ' group by ' + columns + ' having count(1) > 1 '
    # query = 'select ' + columns + ' ,count(1) from ' + table_nm + ' where dt= ' + date_load + ' group by ' + columns + ' having count(1) > 1 '
    query = 'select ' + columns + ' ,count(1) from ' + table_nm + ' group by ' + columns + ' having count(1) > 1 '
    result = data_cursor1.get_data(query)

    f_query.write(" Executed \n \n \n")
    if result:
        err_str_unique = table_nm + "," + ' unique_check ' + "," + ' Table has duplicates records on ' + columns + "\n"
        # f_err.write(err_str_unique)
        print_error(error_warn_unique, err_str_unique)
        write_result_to_db(False, err_str_unique, data_cursor1)
    else:
        suceess_str_unique = table_nm + "," + ' unique_check ' + "," + ' Success ' + "\n"
        f_success.write(suceess_str_unique)
        write_result_to_db(True, suceess_str_unique, data_cursor1)


def get_not_null_error(data_cursor2, table_nm, not_null_column, date_load, date_flg, error_warn_notnull):
    pre = 'count('
    post = ') as cnt_'
    res = [pre + atr + post + atr for atr in not_null_column.split(',')]
    res_str = ','.join(res)

    if date_flg == 1:
        date_type_qry = 'select system$typeof(dt) from ' + table_nm + " where dt = dateadd('day', -1, current_date) limit 1"
        date_type = data_cursor2.get_data(date_type_qry)
        if date_type is None:
            date_type_qry = 'select system$typeof(dt) from ' + table_nm + ' limit 1'
            date_type = data_cursor2.get_data(date_type_qry)
        
        if date_type[0] != 'date':
            res_str_inner_qry = 'select ' + res_str + ' from ' + table_nm + ' where dt= ' + date_load
            query = 'select count(1) from ' + table_nm + ' where dt = ' + date_load
        else:
            res_str_inner_qry = 'select ' + res_str + ' from ' + table_nm + ' where dt= date ' + date_load
            query = 'select count(1) from ' + table_nm + ' where dt = date ' + date_load

    else:
        res_str_inner_qry = 'select ' + res_str + ' from ' + table_nm
        query = 'select count(1) from ' + table_nm
    result = data_cursor2.get_data(query)
    if result[0] > 0:
        result_out = data_cursor2.get_data(res_str_inner_qry)
        f_query.write(" Executed \n \n \n")
        lst = []
        [lst.append(word) for word in not_null_column.split(",")]
        null_columns_lst = []

        count = 0
        for value in result_out:
            if value == 0:
                null_columns_lst.append(lst[count])
            count = count + 1

        null_column_nm = ','.join(null_columns_lst)
        print(null_column_nm)
        if len(null_column_nm) > 1:
            err_str_unique = table_nm + "," + ' not null check ' + "," + ' columns have zero value "' + null_column_nm + '"\n'
            # f_err.write(err_str_unique)
            print_error(error_warn_notnull, err_str_unique)
            write_result_to_db(False, err_str_unique, data_cursor2)

        else:
            success_str_unique = table_nm + "," + ' not null check ' + "," + ' Success ' + "\n"
            f_success.write(success_str_unique)
            write_result_to_db(True, success_str_unique, data_cursor2)


def get_delta_check(data_cursor3, table_nm, max_thrsld_value, hist_date, current_date, error_warn_delta):
    if hist_date[0].isdigit():
        res_hist_dt_str = "'" + hist_date + "'"
    else:
        qry_hist_dt = "select " + hist_date + " as data_load_dt"
        res_hist_dt = data_cursor3.get_data(qry_hist_dt)
        res_hist_dt_str = "'" + str(res_hist_dt[0]) + "'"

    if current_date[0].isdigit():
        res_curr_dt_str = "'" + current_date + "'"
    else:
        qry_curr_dt = "select " + current_date + " as data_load_dt"
        res_curr_dt = data_cursor3.get_data(qry_curr_dt)
        res_curr_dt_str = "'" + str(res_curr_dt[0]) + "'"
    # print res_hist_dt_str
    # print res_curr_dt_str
    date_type_qry = 'select system$typeof(dt) from ' + table_nm + " where dt = dateadd('day', -1, current_date) limit 1"
    date_type = data_cursor3.get_data(date_type_qry)
    if date_type is None:
        date_type_qry = 'select system$typeof(dt) from ' + table_nm + ' limit 1'
        date_type = data_cursor3.get_data(date_type_qry)
    if date_type[0] != 'date':
        res_hist_dt_qry = 'select count(1) from ' + table_nm + ' where dt= ' + res_hist_dt_str
        res_curr_dt_qry = 'select count(1) from ' + table_nm + ' where dt= ' + res_curr_dt_str
    else:
        res_hist_dt_qry = 'select count(1) from ' + table_nm + ' where dt= date ' + res_hist_dt_str
        res_curr_dt_qry = 'select count(1) from ' + table_nm + ' where dt= date ' + res_curr_dt_str
    # print res_hist_dt_qry
    # print res_curr_dt_qry
    res_hist_cnt = data_cursor3.get_data(res_hist_dt_qry)
    res_curr_cnt = data_cursor3.get_data(res_curr_dt_qry)

    val_hist = res_hist_cnt[0]
    val_curr = res_curr_cnt[0]

    if val_hist != 0:
        thrsld_actual = (abs(val_curr - val_hist) / val_hist) * 100
        thrsld_actual_sign = ((val_curr - val_hist) / val_hist) * 100
        print(thrsld_actual)
        max_thrsld_value = int(max_thrsld_value)
        print(max_thrsld_value)
        if thrsld_actual > max_thrsld_value:
            err_str_delta = table_nm + ', delta check , table actual threshold ' + str(
                thrsld_actual_sign) + ' %  exceeded allowed  ' + str(
                max_thrsld_value) + ' % differences for data_date ' + res_hist_dt_str + ' = ' + str(
                val_hist) + ' and ' + res_curr_dt_str + '  = ' + str(val_curr) + '\n'

            print_error(error_warn_delta, err_str_delta)
            write_result_to_db(False, err_str_delta, data_cursor3)
            # f_err.write(err_str_delta)
        else:
            suceess_str_delta = table_nm + "," + ' delta check ' + "," + ' Success ' + "\n"
            f_success.write(suceess_str_delta)
            write_result_to_db(True, suceess_str_delta, data_cursor3)

    else:
        print(' Table has 0 data for history data date ' + res_hist_dt_str)


def get_custom_check(data_cursor4, table_nm, query_custom, custom_qc_desc, err_warn_val):
    result_custom = data_cursor4.get_data(query_custom)
    if result_custom:
        error_custom_strng = table_nm + ', custom check ,' + custom_qc_desc + "\n"
        print_error(err_warn_val, error_custom_strng)
        write_result_to_db(False, error_custom_strng, data_cursor4)

    else:
        success_custom_strng = table_nm + "," + ' custom check ' + "," + ' Success ' + custom_qc_desc + "\n"
        f_success.write(success_custom_strng)
        write_result_to_db(True, success_custom_strng, data_cursor4)


def print_error(err_warn_value, err_str):
    if err_warn_value.lower() == 'error':
        f_err.write(err_str)
    else:
        f_warn.write(err_str)


def print_success(success_str):
    f_success.write(success_str)


def send_mail_as_attachment():
    fromaddr = "info.dp@swiggy.in"
    recipients = ['dp-dataengg-stakeholders@swiggy.in','data-platform@swiggy.in']
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    for addr in recipients:
        msg['To'] = addr
    # print msg
    now = datetime.datetime.now()
    date_time = now.strftime("%Y-%m-%d")
    msg['Subject'] = "Snowflake data state summary for date->" + str(date_time)
    # string to store the body of the mail
    body = " Attached sheets contains data state summary.\n" \
           " Any error reported in 'Error.csv' will be looked by DP team within 3 hours.\n" \
           " If resolution is not provided within 3 hours for errors reported in 'error.csv', DP team would provide ETA to resolve same."
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))
    files = ['error.csv', 'success.csv', 'warning.csv']
    print(files)
    for f in files:
        file_suffix = f.split('/')
        print(f)
        file_nm = file_suffix[-1]
        attachment = MIMEApplication(open(f, "rb").read(), _subtype="txt")
        attachment.add_header('Content-Disposition', 'attachment', filename=file_nm)
        msg.attach(attachment)
    # {'email' : 'info.dp@swiggy.in', 'password' : 'ntloqllnubaowlzz'}
    s = smtplib.SMTP('smtp.gmail.com', 587)
    # Password_of_the_sender = getpass.getpass('Password:')
    # start TLS for security
    s.starttls()
    # Authentication
    s.login(fromaddr, email_password)
    # Converts the Multipart msg into a string
    text = msg.as_string()
    # sending the mail
    s.sendmail(fromaddr, recipients, text)
    # terminating the session
    s.quit()


if __name__ == "__main__":

    # python get_all_error_warn.py table_details.json error.txt queries.txt presto
    # first argument as JSON file name
    # second argument as error file name
    # third argument as success file name
    # fourth argument as warning file name
    # fifth argument as list of all executed queries file name as part of this script
    # sixth argument is mode either presto or snowflake

    f_err = open('error.csv', "w+")
    f_success = open('success.csv', "w+")
    f_warn = open('warning.csv', "w+")
    f_query = open('query.txt', "w+")
    mode = 'snowflake'
    data_cursor = WarehouseCursor(mode=mode)
    # print(data_cursor.get_data("select dateadd('day',-1,current_date)"))
    if mode not in ['presto', 'snowflake']:
        raise ValueError("mode should be either presto or snowflake")
    error_header = ' table_nm ' + "," + ' qc_Check ' + "," + ' qc_desc ' + "\n"
    f_err.write(error_header)

    success_header = ' table_nm ' + "," + ' qc_Check ' + "," + ' qc_desc ' + "\n"
    f_success.write(success_header)

    warn_header = ' table_nm ' + "," + ' qc_Check ' + "," + ' qc_desc ' + "\n"
    f_warn.write(warn_header)

    checks = s1.query(Check).filter(Check.is_active).all()
    for check in checks:
        print(check)
        current_check = check
        # print key
        table_nm = check.table_nm
        schema_nm = check.schema_nm
        info = json.loads(check.info)
        unique_columns = info.get("unique_columns")
        err_warn_value = check.error_warning
        qc_check = check.qc_check
        # not_null_column = info.get("not_null_column")
        # not_null_column_no_partition = not_null_column[:-3]
        has_dt_partition = info.get("has_dt_partition")
        dt_col = 'dt'
        if "date_column" in info and info.get("date_column") != "":
            dt_col = '"date"' if info.get("date_column") == "|date|" else info.get("date_column")
        date_flag = 0

        if has_dt_partition == 'yes':
            date_flag = 1
            print(table_nm)
            data_load_dt_input = info.get("data_load_dt")
            print(info.get("has_dt_partition"))
            print(info.get("not_null_column"))

            print(data_load_dt_input)
            qry_dt = 'select ' + str(data_load_dt_input) + ' as data_load_dt'
            print(qry_dt)
            data_load_dt = data_cursor.get_data(qry_dt)
            data_load_dt_str = "'" + str(data_load_dt[0]) + "'"
            # not_null_column_no_partition = not_null_column[:-3]
        # else:
        # not_null_column_no_partition = not_null_column

        if qc_check.lower() == 'count check':
            get_count_error(data_cursor, table_nm, data_load_dt_str, date_flag, err_warn_value, dt_col)

        if qc_check.lower() == 'unique check':
            get_unique_error(data_cursor, table_nm, unique_columns, data_load_dt_str, date_flag, err_warn_value)

        if qc_check.lower() == 'not null check':
            not_null_column = info.get("not_null_column")
            not_null_column_list = not_null_column.split(',')
            if has_dt_partition == 'yes' and not_null_column_list[-1] == 'dt':
                not_null_column_no_partition = not_null_column[:-3]
            else:
                not_null_column_no_partition = not_null_column
            get_not_null_error(data_cursor, table_nm, not_null_column_no_partition, data_load_dt_str, date_flag,
                               err_warn_value)

        if qc_check.lower() == 'delta check':
            hist_delta_date = info.get("hist_delta_date")
            curr_delta_date = info.get("curr_delta_date")
            thrsld_prcnt_value = info.get("thrsld_prcnt_value")

            get_delta_check(data_cursor, table_nm, thrsld_prcnt_value, hist_delta_date, curr_delta_date,
                            err_warn_value)

        if qc_check.lower() == 'custom check':
            qc_query = info.get("custom_qc_query")
            qc_desc = info.get("custom_qc_desc")
            get_custom_check(data_cursor, table_nm, qc_query, qc_desc, err_warn_value)

    data_cursor.close_con()
    f_err.close()
    f_query.close()
    f_success.close()
    f_warn.close()
    send_mail_as_attachment()
